import os

# replacement strings
WINDOWS_LINE_ENDING = b'\r\n'
UNIX_LINE_ENDING = b'\n'

dir = "../mblender_patches/patches"
dir = "D:/WORK/MBLEND_DOC/WEB/mblender_patches/patches"

def proces (file):			
    with open(file, 'rb') as pfile:
        content = pfile.read()
        
    content = content.replace(WINDOWS_LINE_ENDING, UNIX_LINE_ENDING)

    with open(file, 'wb') as pfile:
        pfile.write(content)

for file in os.listdir(dir):
    proces(dir + "/" + file)