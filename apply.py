
import subprocess
import os
import sys


url = "https://mechanicalblender.org/mblender_patches/patches/"

path = os.path.dirname(__file__)
path = os.path.join(path, '..')
path = blender_path = os.path.join(path, 'blender')
path = mblender_path = os.path.join(path, 'mblender')
path = applied_path = os.path.join(path,"applied")

if not os.path.exists(path):
    os.makedirs(path)

sys.path.append(mblender_path)
import patches

os.chdir (blender_path)


for name in patches.names:
    file = name + ".patch"
    remote = url + file
    local = os.path.join(applied_path, file)
    #curl --ssl-no-revoke https://mechanicalblender.org/mblender_patches/patches/mb-0007-transform-flags.patch > mblender\applied\mb-0007-transform-flags.patch
    with open (local, "wb") as pfile:
        subprocess.run(['curl', '--ssl-no-revoke',  remote], stdout=pfile)
    
    subprocess.run(['git', 'apply', local])
    
