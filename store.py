import subprocess
import os

from datetime import datetime

date_str = datetime.today().strftime('%Y%m%d')

# git rev-parse --abbrev-ref HEAD
result = subprocess.run(['git', 'rev-parse', '--abbrev-ref', 'HEAD'], stdout=subprocess.PIPE)
branch = result.stdout.decode('utf-8').rstrip();

# git rev-parse HEAD
result = subprocess.run(['git', 'rev-parse', 'HEAD'], stdout=subprocess.PIPE)
commit = result.stdout.decode('utf-8').rstrip();

result = subprocess.run(['git', 'log', '-n', '1'], stdout=subprocess.PIPE)
commit_msg = result.stdout;


#git show bf-blender:README.md > README.md
with open ("README.md", "wb") as pfile:
    subprocess.run(['git', 'show', 'bf-blender:README.md'], stdout=pfile)
    
#git diff bf-blender
result = subprocess.run(['git', 'diff', 'bf-blender'], stdout=subprocess.PIPE)
diff = result.stdout

subprocess.run(['git', 'restore', 'README.md'])


loc = "D:\\WORK\\MBLEND_DOC\\WEB\\mblender_patches\\patches\\"

os.chdir(loc)


file = branch+".patch"
file_stored = branch+"."+date_str+".patch"

new = not os.path.isfile(file)

with open(file, 'wb') as pfile:
    pfile.write(diff)

subprocess.run(['git', 'add',  file]);
subprocess.run(['git', 'commit', '-m','commit '+ commit]);

with open('temp', 'wb') as pfile:
    pfile.write(commit_msg)

if new:
    subprocess.run(['svn', 'add', file]);

subprocess.run(['svn', 'commit', '-F', 'temp']);

os.remove('temp');

if os.path.isfile(file_stored):
    subprocess.run(['svn', 'delete', file_stored]);

subprocess.run(['svn', 'copy', file, file_stored]);
subprocess.run(['svn', 'ci', "-m", "store"]);


