import subprocess
import os
import sys



path = os.path.dirname(__file__)
path = os.path.join(path, '..')
path = blender_path = os.path.join(path, 'blender')
path = mblender_path = os.path.join(path, 'mblender')
path = applied_path = os.path.join(path,"applied")

sys.path.append(mblender_path)
import patches

os.chdir(blender_path)

for name in patches.names:
    file = name + ".patch"
    local = os.path.join(applied_path, file)
    if (os.path.isfile (local)):
        subprocess.run(['git', 'apply', '-R', local])
        os.remove(local)

os.remove(os.path.join(mblender_path, "patches.py"))
